$(function () {

    Morris.Donut({
        element: 'mutants-chart',
        data: [{"label":"ActivityNotDefined","value":2},{"label":"ClosingNullCursor","value":2}],
        resize: true
    });

    Morris.Donut({
        element: 'mutants-killed-chart',
        data: [{"label":"Killed mutants","value":1},{"label":"Still alive","value":3}],
        resize: true
    });

    Morris.Donut({
        element: 'mutants-killed-by-monkeys-chart',
        data: [{"label":"Killed mutants","value":1},{"label":"Still alive","value":3}],
        resize: true
    });
    Morris.Donut({
        element: 'monkeys-distribution-chart',
        data: [{"label":"Touch","value":15.306122},{"label":"Motion","value":10.204082},{"label":"Pinch Zoom","value":2.0408163},{"label":"Trackball","value":15.306122},{"label":"Rotation","value":0},{"label":"Navigation","value":0},{"label":"Major Navigation","value":25.510204},{"label":"System Keys","value":15.306122},{"label":"App Switch","value":0},{"label":"Flip","value":2.0408163},{"label":"Any Event","value":1.0204082},{"label":"Z Count","value":13.265306}],
        resize: true
    });





});