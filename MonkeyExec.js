var execute = require('./execute.js');
module.exports = async function (packageName, events, seed) {
    var state = 0;
    var result = {
        eventDist: []
    };
    await execute('adb', ['shell', 'monkey', '-p', packageName, /*'--throttle', '5000',*/ '--pct-syskeys', '0', '-v', events, '-s', seed], {
        onData: onData,
        onError: function (data) {
            console.log('Error', '           ', data);
        }
    });

    return result;

    function onData(data) {
        if (state === 0) {
            eventDistr(data, result.eventDist);
            if (result.eventDist.length === 12) state = 1;

        } else if (state === 1) {
            var crashMessage = isACrash(data);
            if (crashMessage !== null) {
                state = 2;
                result.crash = true;
                result.crashError = crashMessage;
            } else {
                var amount = getEventsInjected(data);
                if (amount !== null) {
                    state = 3;
                    result.crash = false;
                    result.amountEvents = amount;
                }
            }
        } else if (state === 2) {
            var amount = getEventsInjected(data);
            if (amount !== null) {
                state = 3;
                result.amountEvents = amount;
            }
        } else if (state === 3) {
            var expresion = /Network stats: (.*\))/;
            var matches = expresion.exec(data);
            if (matches !== null) {
                state = 4;
                result.networkStats = matches[1];
            }
        }
    }
};



function eventDistr(data, events) {
    var expresion = /(?:((\d+): -?(\d*\.\d*)%).*)+/;
    var matches = expresion.exec(data);
    while (matches !== null) {
        events[parseInt(matches[2])] = parseFloat(matches[3]);
        data = data.replace(matches[1], '');
        matches = expresion.exec(data);
    }
    return events;
}

function isACrash(data) {
    var expresion = /CrashReportActivity/;
    var matches = expresion.exec(data);
    if (matches !== null) {
        return matches[0];
    } else {
        return matches;
    }
}

function getEventsInjected(data) {
    var expresion = /Events injected: (\d*)/;
    var matches = expresion.exec(data);
    if (matches !== null) {
        return matches[1];
    } else {
        return matches;
    }
}