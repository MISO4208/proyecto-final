var fs = require('fs-extra');
var path = require('path');
const execute = require('./execute.js');
var exports = module.exports = {};

exports.build = async function (element, testFolder, packageName, scPath, mfPath, calabashTestFolder) {
    // console.log(element, testFolder, packageName, scPath, mfPath)
    console.log('Copy start');
    fs.removeSync(mfPath);
    fs.ensureDirSync(mfPath);
    fs.copySync(path.join(testFolder, 'mutants', packageName + '-mutant' + element.mutantID), mfPath);
    console.log('Copy finished');
    console.log('Building mutant' + element.mutantID + ' started');
    await execute('./gradlew', ['assembleProd'], {
        cwd: scPath
    });
    console.log('Build finished');
    console.log('copying apk of mutant' + element.mutantID);
    const generatedAPK = packageName + '-mutant' + element.mutantID + '.apk';
    fs.copySync(path.join(scPath, 'app', 'build', 'outputs', 'apk', 'prod', 'debug', 'app-prod-debug.apk'), path.join(calabashTestFolder, generatedAPK));
    return generatedAPK;
};