$(function () {

    Morris.Donut({
        element: 'mutants-chart',
        data: [{"label":"ActivityNotDefined","value":2},{"label":"ClosingNullCursor","value":3}],
        resize: true
    });

    Morris.Donut({
        element: 'mutants-killed-chart',
        data: [{"label":"Killed mutants","value":1},{"label":"Still alive","value":4}],
        resize: true
    });

});