var exec = require('child_process').spawn;

module.exports = function execute(command, params, options) {
    options = options || {};
    let count = 0;
    return new Promise((resolve) => {
        const child = exec(command, params, {
            cwd:options.cwd||__dirname
        });
        child.stdout.on('data', data => {
            if (options.onData) options.onData(data.toString());
            print();
        });

        child.stderr.on('data', data => {
            if (options.onError) options.onError(data.toString());
            print();
        });

        child.on('close', function (code) {
            resolve(code);
        });
    });

    function print() {
        count++;
        if (count > 400) {
            count = 0;
            process.stdout.write('.');
        }
    }
};