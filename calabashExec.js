const execute = require('./execute');
const regexResult = /(Failing Scenarios:((\n.*)*)\n)?([0-9]*)+ scenarios? \(.*(([0-9]+) (passed|failed))\)/;

//(Failing Scenarios:((\n.*)*)\n)?([0-9]*)+ scenarios \(.*(([0-9]+) (passed|failed))\)

module.exports = async function (calabashTestFolder, apkName) {
    let scenarios;
    let passed;
    let errors;
    //readConsole('5 scenarios (3 failed, 2 passed)');
    await execute('calabash-android', ['resign', apkName], {
        onData: readConsole,
        onError: readConsole,
        cwd: calabashTestFolder
    });
    await execute('calabash-android', ['run', apkName], {
        onData: readConsole,
        onError: readConsole,
        cwd: calabashTestFolder
    });
    return {
        scenarios,
        passed,
        errors
    };

    function readConsole(data) {
        console.log(data);
        const result = data.match(regexResult);
        if (result) {
            errors = result[1];
            scenarios = result[4];
            passed = result[7] === 'passed' ? result[6] : scenarios - result[6];
        }
    }
};