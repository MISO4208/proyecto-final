const fs = require('fs-extra');
const path = require('path');

const indexTemplatePath = path.join('report', 'index.tmp.html');
const indexPath = path.join('report', 'index.html');

const charDataTemplatePath = path.join('report', 'morris-data.tmp.js');
const charDataPath = path.join('report', 'morris-data.js');

const saltoLineaRE = /\n/g;

const monkeyCommandTypes = ['Touch', 'Motion', 'Pinch Zoom', 'Trackball', 'Rotation', 'Navigation', 'Major Navigation', 'System Keys', 'App Switch', 'Flip', 'Any Event', 'Z Count'];

/*generate('wikipedia.org', [{
    mutantID: '41',
    pathToFile: '/Users/jose/Documents/DatosMac/Jose/Universidad/2017-20/pruebas/proyecto-final/wikipedia/app/src/main/java/org/wikipedia/readinglist/ReadingListData.java',
    mutantType: 'ClosingNullCursor',
    mutantLine: '44',
    killed: true,
    stack: '\ncucumber features/search.feature:15 # Scenario Outline: As user I want to search some words, Examples (#1)\ncucumber features/search.feature:28 # Scenario Outline: As user I want to see my search history, Examples (#1)\n'
}, {
    mutantID: '41',
    pathToFile: '/Users/jose/Documents/DatosMac/Jose/Universidad/2017-20/pruebas/proyecto-final/wikipedia/app/src/main/java/org/wikipedia/readinglist/ReadingListData.java',
    mutantType: 'ClosingNullCursor',
    mutantLine: '44',
    killed: true,
    stack: '\ncucumber features/search.feature:15 # Scenario Outline: As user I want to search some words, Examples (#1)\ncucumber features/search.feature:28 # Scenario Outline: As user I want to see my search history, Examples (#1)\n'

}, {
    mutantID: '41',
    pathToFile: '/Users/jose/Documents/DatosMac/Jose/Universidad/2017-20/pruebas/proyecto-final/wikipedia/app/src/main/java/org/wikipedia/readinglist/ReadingListData.java',
    mutantType: 'ClosingNullCursor',
    mutantLine: '44',
    killed: false
}], {
    ActivityNotDefined: {
        amount: 29,
        startIndex: 0,
        endIndex: 28
    },
    ClosingNullCursor: {
        amount: 16,
        startIndex: 29,
        endIndex: 44
    }
});*/

module.exports = function (appName, mutants) {
    const killedMutants = mutants.filter(mutant => mutant.killed);
    const unkilledMutants = mutants.filter(mutant => !mutant.killed);
    const killedMutantsByMonkey = mutants.filter(mutant => mutant.monkeyResult.crash);

    const mutantsDict = {};
    mutants.forEach(mutant => {
        if (mutantsDict[mutant.mutantType] === undefined) {
            mutantsDict[mutant.mutantType] = 1;
        } else {
            mutantsDict[mutant.mutantType]++;
        }
    });


    const indexContent = fs.readFileSync(indexTemplatePath)
        .toString()
        .replace('{{appName}}', appName)
        .replace('{{appName}}', appName)
        .replace('{{numMutants}}', mutants.length)
        .replace('{{killPercentage}}', parseInt(killedMutants.length / mutants.length * 100))
        .replace('{{killedMutants}}', killedMutants.length)
        .replace('{{unkilledMutants}}', unkilledMutants.length)
        .replace('{{unkilledMutantsListHtml}}', mutantsToHtml(unkilledMutants))
        .replace('{{killedMutantsListHtml}}', mutantsToHtml(killedMutants))
        .replace('{{killedMutantsByMonkey}}', killedMutantsByMonkey.length)
        .replace('{{killPercentageMonkeys}}', parseInt(killedMutantsByMonkey.length / mutants.length * 100))
        .replace('{{killedMutantsByMonkeyListHtml}}', mutantsMonkeyToHtml(killedMutantsByMonkey));



    fs.writeFileSync(indexPath, indexContent);



    const chartDataContent = fs.readFileSync(charDataTemplatePath)
        .toString()
        .replace('--mutantsChartData--', JSON.stringify(Object.keys(mutantsDict).map(mutantType => ({
            label: mutantType,
            value: mutantsDict[mutantType]
        }))))
        .replace('--mutantsKilledChartData--', JSON.stringify([{
            label: 'Killed mutants',
            value: killedMutants.length
        }, {
            label: 'Still alive',
            value: unkilledMutants.length
        }]))
        .replace('--mutantsKilledByMonkeysChartData--', JSON.stringify([{
            label: 'Killed mutants',
            value: killedMutantsByMonkey.length
        }, {
            label: 'Still alive',
            value: mutants.length - killedMutantsByMonkey.length
        }]))
        .replace('--monkeysDistributionChartData--', JSON.stringify(mutants[0].monkeyResult.eventDist.map((percentage, i) => ({
            label: monkeyCommandTypes[i],
            value: percentage
        }))));
    fs.writeFileSync(charDataPath, chartDataContent);


};

function mutantsToHtml(mutants) {
    return mutants.map(mutant => `
        <a href="file://${mutant.pathToFile}" class="list-group-item">
            <i class="fa fa-${mutant.killed?'bolt':'warning'} fa-fw"></i> 
            ${mutant.mutantID}. ${mutant.mutantType}
            <span class="pull-right text-muted small">
                <em>Line ${mutant.mutantLine}</em>
            </span>
            <p class="small">
                ${mutant.stack?mutant.stack.replace(saltoLineaRE, '<br>'):''}
            </p>
        </a>
    `).join('\n');
}

function mutantsMonkeyToHtml(mutants) {
    return mutants.map(mutant => `
        <a href="file://${mutant.pathToFile}" class="list-group-item">
            <i class="fa fa-${mutant.killed?'bolt':'warning'} fa-fw"></i> 
            ${mutant.mutantID}. ${mutant.mutantType}
            <span class="pull-right text-muted small">
                <em>Line ${mutant.mutantLine}</em>
            </span>
            <p class="small">
                Events: ${mutant.monkeyResult.amountEvents} <br>
                ${mutant.monkeyResult.crashError?mutant.monkeyResult.crashError.replace(saltoLineaRE, '<br>'):''}<br>
                Network Stats ${mutant.monkeyResult.networkStats}
            </p>
        </a>
    `).join('\n');
}