var fs = require('fs');
var exports = module.exports = {};

exports.parseFile = function (path) {
    var text = fs.readFileSync(path);
    var lines = text.toString().split('\n');
    var expresion = /Mutant (\d+): (.*); (\w+) in line (\d+)/;
    var results = [];
    var dictionary = {};
    for (let index = 0; index < lines.length - 1; index++) {
        const element = lines[index];
        var matches = expresion.exec(JSON.stringify(element));
        results[index] = {};
        results[index]['mutantID'] = matches[1];
        results[index]['pathToFile'] = matches[2];
        results[index]['mutantType'] = matches[3];
        results[index]['mutantLine'] = matches[4];
        if (dictionary[results[index]['mutantType']] == undefined) {
            dictionary[results[index]['mutantType']] = {
                'amount': 1,
                'startIndex': index,
                'endIndex': index
            };
        } else {
            dictionary[results[index]['mutantType']]['amount']++;
            dictionary[results[index]['mutantType']]['endIndex'] = index;
        }
    }
    return {
        results,
        dictionary
    };
};