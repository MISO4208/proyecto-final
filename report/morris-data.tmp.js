$(function () {

    Morris.Donut({
        element: 'mutants-chart',
        data: --mutantsChartData--,
        resize: true
    });

    Morris.Donut({
        element: 'mutants-killed-chart',
        data: --mutantsKilledChartData--,
        resize: true
    });

    Morris.Donut({
        element: 'mutants-killed-by-monkeys-chart',
        data: --mutantsKilledByMonkeysChartData--,
        resize: true
    });
    Morris.Donut({
        element: 'monkeys-distribution-chart',
        data: --monkeysDistributionChartData--,
        resize: true
    });





});