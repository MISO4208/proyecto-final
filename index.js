var fs = require('fs-extra');
const mdp = require('./MDroidPlusParser.js');
const mb = require('./MutantBuilder.js');
const path = require('path');
const mde = require('./MDroidPlusExec.js');
const calabashExec = require('./calabashExec.js');
const MonkeyExec = require('./MonkeyExec');
const report = require('./report.js');
const argv = require('argv');

var args = argv.option([{
        short: 'c',
        name: 'source-code-path',
        type: 'path',
        description: 'Defines the path to the source code'
    }, {
        short: 'm',
        name: 'main-folder-path',
        type: 'string',
        description: 'Defines the path to the src/main folder'
    }, {
        short: 'p',
        name: 'package-name',
        type: 'string',
        description: 'Defines the package name of the app'
    }, {
        short: 'o',
        name: 'mdroid-mutation-file',
        type: 'path',
        description: 'Defines the path to the MDroid+ file that defines the wanted mutants'
    }, {
        short: 'f',
        name: 'test-dest-folder',
        type: 'path',
        description: 'Defines the path where the user wants to be stored the result, if it is not defined the default folder is ~/testResults'
    }, {
        short: 't',
        name: 'calabash-test-folder',
        type: 'path',
        description: 'Defines the path where the calabash test suite is defined'
    }, {
        short: 'a',
        name: 'alpha',
        type: 'int',
        description: 'Defines the probability alpha of testing a mutant'
    }, {
        short: 'v',
        name: 'events',
        type: 'int',
        description: 'Defines the number of events executed by the monkey'
    }, {
        short: 's',
        name: 'seed',
        type: 'int',
        description: 'Defines the seed for pseudo-aleatory generation of monkey events'
    }



]).run();

var packageName = args.options['package-name'];
var mdroidMutationFile = args.options['mdroid-mutation-file'];
var mainFolderPath = args.options['main-folder-path'];
var sourceCodePath = args.options['source-code-path'];
var calabashTestFolder = args.options['calabash-test-folder'];
var alpha = args.options['alpha'];

var events = args.options['events'];
var seed = args.options['seed'];

var rslt = '';
if (packageName === undefined) {
    rslt += 'Package name missing \n';
}
if (mainFolderPath === undefined) {
    rslt += 'main-folder-path missing \n';
}
if (sourceCodePath === undefined) {
    rslt += 'source-code-path missing \n';
}
if (calabashTestFolder === undefined) {
    rslt += 'calabash-test-folder missing \n';
}
if (alpha === undefined) {
    rslt += 'alpha missing \n';
}
if (rslt !== '') {
    console.log(rslt);
    return;
}
alpha = alpha / 100;
if (events === undefined) {
    events = 10000;
}
if (seed === undefined) {
    seed = parseInt(Math.random() * 10000000) + 1000000;
}

if (!process.env.ANDROID_HOME) {
    throw new Error('You sould define ANDROID_HOME environment variable.');
}
if (mdroidMutationFile === undefined) {
    console.log('mdroid-mutation-file missing, default file will be used.');
    mdroidMutationFile = './MDroid';
}

if (!fs.existsSync(sourceCodePath)) {
    console.log('Source folder does not exists');
    return;
}
mainFolderPath = path.join(sourceCodePath, mainFolderPath);
if (!fs.existsSync(mainFolderPath)) {
    console.log('Main source folder does not exists', mainFolderPath);
    return;
}
if (!fs.existsSync(mdroidMutationFile)) {
    console.log('MDroid+ main file\'s folder does not exists');
    return;
}
if (!fs.existsSync(calabashTestFolder)) {
    console.log('Calabash test definition folder does not exists', calabashTestFolder);
    return;
}
if (!fs.existsSync(sourceCodePath + '/gradlew')) {
    console.log('Source Code main folder is not an android Studio project');
    return;
}

var testFolder = path.join(__dirname, 'testResult');
var testDestFolder = args.options['test-dest-folder'];
fs.ensureDirSync(testDestFolder);
async function initialize() {
    if (testDestFolder != undefined && fs.existsSync(testDestFolder)) {
        console.log('All test files will be generated in ' + testDestFolder);
        if (fs.existsSync(path.join(testDestFolder, 'testResult'))) {
            var files = fs.readdirSync(path.join(testDestFolder, 'testResult'));
            if (files.length > 0) {
                console.log('There are already files in this folder, they will be deleted and the test results will be save instead of them');
                fs.removeSync(path.join(testDestFolder, 'testResult'));
            }
        }
        testFolder = path.join(testDestFolder, 'testResult');
    }
    fs.ensureDirSync(testDestFolder);
    fs.ensureDirSync(testFolder);
    fs.ensureDirSync(path.join(testFolder, 'mutants'));
    fs.ensureDirSync(path.join(testFolder, 'apks'));
    fs.ensureDirSync(path.join(testFolder, 'report'));

    // fs.ensureDirSync(testFolder + "/baseCodeMutant")
    // console.log('Copying source code');
    // fs.copySync(sourceCodePath, testFolder + "/baseCodeMutant");
    // console.log('End copying source code');
}

async function startProcess() {
    await initialize();
    console.log('Start MDroid+ mutant generation');
    await mde(mainFolderPath, packageName, path.join(testFolder, 'mutants'), mdroidMutationFile);
    console.log('Finish MDroid+ mutant generation');
    console.log('Processing MDroid+ output');
    var json = mdp.parseFile(path.join(testFolder, 'mutants', packageName + '-mutants.log'));
    console.log('Finished processing mDroid+ output');
    //var keys = Object.keys(json.dictionary);
    //var selectedCategory = json.dictionary[keys[value]];
    console.log("json", json);
    console.log('Start mutants building process');
    const mutantsProcessed = [];

    for (let index = 0; index < json.results.length; index++) {

        if (Math.random() < alpha) {
            const element = json.results[index];
            // console.log(index-selectedCategory.startIndex, element)
            const apkName = await mb.build(element, testFolder, packageName, sourceCodePath, mainFolderPath, calabashTestFolder);
            const result = await calabashExec(calabashTestFolder, apkName);
            element.killed = result.passed !== result.scenarios;
            element.stack = result.errors;

            element.monkeyResult = await MonkeyExec(packageName, events, seed);
            console.log('element.monkeyResult', element.monkeyResult);


            mutantsProcessed.push(element);
        }
    }

    report(packageName, mutantsProcessed);



}
try {
    startProcess();
} catch (e) {
    throw e;
}